<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('var')
    ->exclude('vendor')
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@PSR1' => true,
        '@PSR12' => true,
        'no_unused_imports' => true,
        'phpdoc_no_useless_inheritdoc' => true,
        'no_empty_comment' => true,
        'no_empty_phpdoc' => true,
        'align_multiline_comment' => true,
        'single_line_throw' => false,
        'ordered_imports' => ["sort_algorithm" => "alpha"],
    ])
    ->setFinder($finder)
    ;