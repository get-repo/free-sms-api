<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/39577018/free_logo.png" height=100 />
</p>

<h1 align=center>Free SMS API</h1>

<br/>

A PHP client to send SMS to free mobile to yourself.

## Table of Contents

1. [Installation](#installation)
1. [Example](#example)

## Installation

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/free-sms-api git https://gitlab.com/get-repo/free-sms-api.git
    composer require get-repo/free-sms-api

## Example

```php
use GetRepo\FreeSmsApi\FreeSmsApiSender;

$client = new FreeSmsApiSender(
    accountId: '123456',
    password: 'p@$$w0rd',
);
if ($client->send('txt me !')) {
    echo 'it worked !';
} else {
    echo 'bad auth ?';
}

```