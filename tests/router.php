<?php

// check login
if (
    !isset($_GET['user'])
    || !isset($_GET['pass'])
    || 'user' !== $_GET['user']
    || 'pass' !== $_GET['pass']
) {
    http_response_code(403);
    exit;
}

// check message
if (!isset($_GET['msg']) || !$_GET['msg']) {
    http_response_code(400);
    exit;
}

// success
http_response_code(200);
