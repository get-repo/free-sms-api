<?php

namespace Test;

use GetRepo\FreeSmsApi\FreeSmsApiSender;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Panther\ProcessManager\WebServerManager;

class FreeSmsApiSenderTest extends TestCase
{
    private const HOST = '127.0.0.1';
    private const PORT = 8085;

    private static WebServerManager $webServerManager;

    public static function setUpBeforeClass(): void
    {
        self::$webServerManager = new WebServerManager(
            __DIR__,
            self::HOST,
            self::PORT,
            __DIR__ . '/router.php'
        );
        self::$webServerManager->start();
    }

    public static function tearDownAfterClass(): void
    {
        self::$webServerManager->quit();
    }

    public function testSendSuccess(): void
    {
        $freeSmsApiSender = $this->getFreeSmsApiSender('user', 'pass');
        $this->assertTrue($freeSmsApiSender->send('should be sent'));
    }

    public function testSendFailed(): void
    {
        $freeSmsApiSender = $this->getFreeSmsApiSender('whatever', 'whatever');
        $this->assertFalse($freeSmsApiSender->send('should fail'));
    }

    private function getFreeSmsApiSender(string $accountId, string $password): FreeSmsApiSender
    {
        return new FreeSmsApiSender($accountId, $password, sprintf('http://%s:%s', self::HOST, self::PORT));
    }
}
