<?php

namespace GetRepo\FreeSmsApi;

class FreeSmsApiSender
{
    final public const API_ENDPOINT = 'https://smsapi.free-mobile.fr';

    public function __construct(
        private readonly string $accountId,
        private readonly string $password,
        private readonly string $endpoint = self::API_ENDPOINT, // for testing purpose only
    ) {
    }

    public function send(string $message): bool
    {
        $url = sprintf(
            '%s/sendmsg?user=%s&pass=%s&msg=%s',
            $this->endpoint,
            $this->accountId,
            urlencode($this->password),
            urlencode($message),
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return 200 === $code;
    }
}
